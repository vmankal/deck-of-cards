# Deck of cards

Simple project to shuffle a deck of cards and deal 'n' number of cards to 'n' players

# To run the program

* Modify the index.js file to add/remove players and to set the number of cards to be dealt to each player
* Run `npm run start`