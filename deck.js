
class Deck {
  constructor(cards) {
    console.log('Deck Initiated');

    this.cards = cards;
  }

  getCards() {
    return this.cards;
  }

  shuffle() {
    const result = [];
    for (let i = this.cards.length; i > 0; i -= 1) {
      const cardIndex = Deck.between(0, i);
      result.push(this.cards[cardIndex]);
      this.cards.splice(cardIndex, 1);
    }
    this.cards = result;
    return result;
  }

  static between(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }
}

module.exports = Deck;
