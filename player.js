
class Player {
  constructor(playerName) {
    this.playerName = playerName;
    this.cards = [];
  }

  deal(card) {
    this.cards.push(card);
  }
}

module.exports = Player;
