
class Game {
  constructor(deck) {
    this.deck = deck;
    this.players = [];
  }

  addPlayer(playerName) {
    this.players.push(playerName);
  }

  deal(cardsPerPlayer) {
    for (let i = 0; i < cardsPerPlayer; i += 1) {
      this.players.forEach((x) => {
        if (this.deck.cards.length > 0) {
          x.deal(this.deck.cards[0]);
          this.deck.cards.splice(0, 1);
        }
      });
    }
    return this.players;
  }
}

module.exports = Game;
