const composition = require('./composition');

const { game, Player } = composition.compose();

game.addPlayer(new Player('Player0'));
game.addPlayer(new Player('Player1'));
game.addPlayer(new Player('Player2'));

game.deal(3);

game.players.forEach((x) => {
  console.log(`Player Name: ${x.playerName}`);
  console.log('==============================================');
  console.log(x.cards);
  console.log('==============================================');
});

console.log(game.deck.cards.length);

game.deal(1);

console.log(game.deck.cards.length);

game.deal(1);

console.log(game.deck.cards.length);
