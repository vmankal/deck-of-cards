
const Game = require('./game');
const Player = require('./player');
const Card = require('./card');
const Deck = require('./deck');


class Root {
  static compose() {
    const suites = [' Hearts', ' Spades', ' Clubs', ' Diamonds'];
    const numbers = ['Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King'];
    const cards = [];
    suites.forEach((s) => {
      numbers.forEach((n) => {
        cards.push(new Card(s, n));
      });
    });
    const deck = new Deck(cards);
    deck.shuffle();
    const game = new Game(deck);
    return { game, Player };
  }
}
module.exports = Root;
